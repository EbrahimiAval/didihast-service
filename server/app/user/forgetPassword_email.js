import catcher from "../../middleware/catcher";
import User from "../../model/User";


async function forgetPassword_email(req, res) {
    const {email} = req.body;
    const user = await User.findOne({email})
    
    function sendRes() {
        res.send('mail sent successfully.')
    }
    
    if (!user)// To prevent the disclosure of information
        sendRes()
        
    await user.sendForgetPasswordLink(req)
              .catch(req.setError(500, 'can not send email.', true))
    
    sendRes()
}


export default catcher(forgetPassword_email)