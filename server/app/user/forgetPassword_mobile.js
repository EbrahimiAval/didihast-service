import catcher from "../../middleware/catcher";
import User from "../../model/User";

async function forgetPassword_mobile(req, res) {
    const {mobile} = req.body;
    const user = await User.findOne({mobile})
    
    if (!user)
        res.status(403).send('user not found')
    
    const verifyHash = user.verifyHash()
    
    await res.json({verifyHash})
}

export default catcher(forgetPassword_mobile)