import catcher from "../../middleware/catcher";
import User from "../../model/User";
import bcryptjs from "bcryptjs";

async function changePassword(req, res) {
    const {password, newPassword} = req.body
    
    if (password === newPassword)
        req.setError(405, 'password and new password is can not be equal!')()
    
    const {userId} = req.session
    const user = await User.findById({_id: userId})
    
    const trustPass = await bcryptjs.compare(password, user.password)
    
    if (!trustPass)
        req.setError(403, 'password is not currect!')()
    
    await user.changePassword(newPassword)
    
    res.send('password changed successfully.')
}

export default catcher(changePassword)