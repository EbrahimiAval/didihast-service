import User from "../../model/User";
import catcher from "../../middleware/catcher";
import initSession from "../../component/initSession";
import {TOKEN_COOKIE_NAME, TOKEN_COOKIE_OPTIONS} from "../../setup/constant";



async function register(req, res) {
    const {mobile, email, password} = req.body;
    // create user
    const user = new User({mobile, email, password});
    const savedUser = await user.save()
                                .catch(req.setError(500, 'can not save user!', true))
    
    savedUser.sendEmailVerifyLink(req)
             .catch((e) => console.error(e))
    
    const session = await initSession(savedUser._id)
        .catch(req.setError(403, 'user saved but session errored!', true))
    
    
    res.cookie(TOKEN_COOKIE_NAME, session.token, TOKEN_COOKIE_OPTIONS)
       .json({csrfToken: session.csrfToken})
    
}

export default catcher(register)
