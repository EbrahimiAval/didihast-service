import bcryptjs from "bcryptjs";
import User from "../../model/User";
import catcher from "../../middleware/catcher";
import logingin from "../../component/logingin";


async function login(req, res) {
    const {email, mobile, password} = req.body;
    const EMAIL_ENTERED = !!email;
    const invalidParamsError = req.setError(401, 'email or password is invalid.')
    
    const username = EMAIL_ENTERED ? {email} : {mobile}
    const user = await User.findOne(username)
    
    if (!user)
        invalidParamsError()
    
    const trustPass = await bcryptjs.compare(password, user.password)
    
    if (!trustPass)
        invalidParamsError()
    
    await logingin(res, user)
}

export default catcher(login)