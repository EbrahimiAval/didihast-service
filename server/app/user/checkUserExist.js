import User from "../../model/User";
import catcher from "../../middleware/catcher";


async function checkUserExist(req, res) {
    const {mobile, email} = req.body;
    const username = mobile ? {mobile} : {email}
    
    await User.exists(username)
              .then(function (isExist) {
                  if (!isExist)
                      throw new Error('user not exist!')
              })
              .catch(req.setError(403))
    
    res.send('user exist')
}

export default catcher(checkUserExist)