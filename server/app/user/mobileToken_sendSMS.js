import catcher from "../../middleware/catcher";
import MobileToken from "../../model/MobileToken";
import Kavenegar from "kavenegar";

async function mobileToken_sendSMS(req, res) {
    const {mobile,template} = req.body;
    const token = MobileToken.generateToken();
    
    // SMS
    Kavenegar
        .KavenegarApi({apikey: process.env.KAVENEGAR})
        .VerifyLookup(
            {
                receptor: mobile,
                token:    token,
                template: template
            },
            async (response, status) => {
                if (status === 200){
                    const mobileToken = new MobileToken({token, mobile});
                    await mobileToken.save()
                                     .catch(req.setError(500, 'can not save mobile token!', true))
                    res.send('sms sent successfully')
                }
                else {
                    res.status(status).send(response)
                }
            }
        )
}

export default catcher(mobileToken_sendSMS)