import User from "../../model/User";
import catcher from "../../middleware/catcher";

async function detail_update(req, res) {
 
    const {userId} = req.session
    const user = await User.findById({_id: userId})
    
    await user.detailUpdate(req.body)
    
    res.send('successfully updated')
}

export default catcher(detail_update)
