import User from "../../model/User";
import catcher from "../../middleware/catcher";



async function emailVerify_submit(req, res) {
    const {userId} = req.session
    const user = await User.findById({_id: userId})
    
    if (user.emailVerified)
        req.setError(402, 'email previously verifed!')()
    
    const hash = req.body.verifyHash;
    const validHash = user.verifyHash('email')
    
    if (hash !== validHash)
        req.setError(403, 'verify hash is not valid.')()
    
    await user.setEmailVerified()
    
    res.status(200).send('email verified successfully.')
}

export default catcher(emailVerify_submit)

