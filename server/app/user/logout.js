import logingout from "../../component/logingout";
import catcher from "../../middleware/catcher";

async function logout(req, res) {
    await logingout(req, res)
    res.send('Successfuly expired login session')
}

export default catcher(logout)