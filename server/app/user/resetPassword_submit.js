import catcher from "../../middleware/catcher";
import User from "../../model/User";
import logingin from "../../component/logingin";


async function resetPassword_submit(req, res) {
    const user = await User.verifingResetPasswordToken(req)
    const {newPassword} = req.body;
    
    await user.changePassword(newPassword)
    
    await logingin(res, user)
}

export default catcher(resetPassword_submit)

