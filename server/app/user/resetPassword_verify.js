import User from "../../model/User";
import catcher from "../../middleware/catcher";

async function resetPassword_verify(req, res) {
    await User.verifingResetPasswordToken(req)
    
    res.send("is valid")
}

export default catcher(resetPassword_verify)