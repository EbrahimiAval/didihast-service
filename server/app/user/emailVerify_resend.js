import catcher from "../../middleware/catcher";
import User from "../../model/User";

async function emailVerify_resend(req, res) {
    const {userId} = req.session
    const user = await User.findById({_id: userId})
    await user.sendEmailVerifyLink(req)
              .catch(req.setError(500, 'can not send mail!', true))
    
    res.send('email sent successfully.')
}

export default catcher(emailVerify_resend)
