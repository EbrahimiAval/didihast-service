import User from "../../model/User";
import catcher from "../../middleware/catcher";

async function detail_get(req, res) {
    const {userId} = req.session
    let savedUser = await User.findById({_id: userId}, {_id: 0, __v: 0, password: 0})
                           .catch(req.setError(401, 'Not authorized.'))
    
    // savedUser = savedUser.toJSON({ virtuals: true }) // add fullName
    
    res.json(savedUser)
}

export default catcher(detail_get)
