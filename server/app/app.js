import express from 'express';
import cookieParser from 'cookie-parser';
import morgan from 'morgan';
import cors from "cors";
import {IS_PRODUCTION, PUBLIC_PATH} from "../setup/constant";
import userRouter from "../setup/router/userRouter";
import dbConnect from "../setup/dbConnect";
import error404 from "./error404/error404";
import setError from "../middleware/setError";
import errorHandler from "../middleware/errorHandler";
import ticketRouter from "../setup/router/ticketRouter";


const app = express();
dbConnect();


const morganOptions = {}
if (IS_PRODUCTION)
    morganOptions.skip = function (req, res) { return res.statusCode < 400 }


const corsOptions = {
    origin:               process.env.SITE_URL,
    methods:              'GET,PUT,POST,DELETE,PATCH',
    credentials:          true,
    optionsSuccessStatus: 200
}


app.use(morgan('dev', morganOptions)) // HTTP request logger middleware for node.js
   .use(cors(corsOptions)) // cross origin
   .use(cookieParser()) // can access to cookie from 'req' as object. like: req.cookies.token
   .use(express.json())  // can get POST request params
   .use(express.urlencoded({extended: false})) // the URL-encoded data will instead be parsed with the querystring library
   .use(express.static(PUBLIC_PATH))
   .use(setError)
   // --------- Routes --------------
   .use('/user', userRouter)
   .use('/ticket', ticketRouter)
   .use('*', error404)
   .use(errorHandler)

export default app;
