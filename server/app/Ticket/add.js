import catcher from "../../middleware/catcher";
import Ticket from "../../model/Ticket";

async function add(req, res) {
    const {subject, description} = req.body
    const {userId} = req.session
    
    const newTicket = {subject, userId, comments: [ {description} ]}
    const ticket = new Ticket(newTicket)
    
    const savedTicket = await ticket.save()
                                    .catch(req.setError(500, 'can not save ticket!', true))
    
    await res.json(savedTicket)
}

export default catcher(add)
