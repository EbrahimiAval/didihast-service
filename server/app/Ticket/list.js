import catcher from "../../middleware/catcher";
import Ticket from "../../model/Ticket";

async function list(req, res) {
    const {userId} = req.session
    
    const tickets = await Ticket.find({userId}, {__v: 0, userId: 0, comments: 0})
    
    await res.json(tickets)
}

export default catcher(list)