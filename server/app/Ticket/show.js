import catcher from "../../middleware/catcher";
import Ticket from "../../model/Ticket";

async function show(req, res) {
    const {ticketId} = req.params
    const {userId} = req.session
    
    const tickets = await Ticket.find({_id: ticketId, userId})
    
    await res.json(tickets)
}

export default catcher(show)