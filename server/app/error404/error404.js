export default function error404(req, res) {
    res.status(404).send("ERROR 404: route not found!");
}