import path from "path";

const {NODE_ENV, SITE_URL} = process.env;

// public directory
export const PUBLIC_PATH = path.join(__dirname, '../public');


// remove https and http from URL. for example get "site.com" from "https://site.com"
export const PURE_SITE_URL = SITE_URL.split('https://')[1] || SITE_URL.split('http://')[1]


// ENV flags
export const IS_DEVELOPMENT = NODE_ENV === 'development';
export const IS_PRODUCTION = NODE_ENV === 'production';


// user token
export const TOKEN_EXPIRE = 1000 * 60 * 60 * 24 * 30; // 1 month as milisconds
export const TOKEN_COOKIE_NAME = 'token'
export const TOKEN_COOKIE_OPTIONS = {
    httpOnly: false,
    sameSite: IS_PRODUCTION ? 'None' : true,
    domain:   IS_PRODUCTION ? PURE_SITE_URL : false,
    maxAge:   TOKEN_EXPIRE, // to can logouting from
    secure:   IS_PRODUCTION
}

// mobile token
export const MOBILE_TOKEN_EXPIRE = 1000 * 60 * 3; // 3 minutes
export const MOBILE_TOKEN_LENGTH = 5; // 5 digit

// user password
export const MIN_PASSWORD_LENGTH = 6;