export default function forgetPasswordTemplate (link) {
    return (`
       <div style="font-family: tahoma;direction: rtl;">
           <h1>بازیابی رمزعبور حساب دیدی هست </h1>
           برای بازیابی رمز عبور حساب کاربری بر روی لینک زیر کلیک نمایید و در غیر این صورت این ایمیل را نادیده بگیرید:
           <br/>
           <a target="_blank" href="${link}">
                <b>بازیابی رمز عبور</b>
           </a>
           <hr/>
           <a target="_blank" href="https://didihast.com"> didihast.com </a>
       </div>             
    `);
};