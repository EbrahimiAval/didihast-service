
export default function emailVerifyTemplate (link) {
    return `
       <div style="font-family: tahoma;direction: rtl;">
           <h1>تایید حساب دیدی هست</h1>
           برای تایید حساب کاربری بر روی لینک زیر کلیک نمایید و در غیر این صورت این ایمیل را نادیده بگیرید:
           <br/>
           <a target="_blank" href="${link}">
                <b>تایید ایمیل</b>
           </a> 
           <hr/>
           <a target="_blank" href="https://didihast.com"> didihast.com </a>
       </div>             
    `;
};