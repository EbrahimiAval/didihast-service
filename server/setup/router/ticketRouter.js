import {Router} from 'express';
import {cookie, header} from "express-validator";
import authentication from "../../middleware/authentication";
import validator from "../../middleware/validator";
import list from "../../app/Ticket/list";
import show from "../../app/Ticket/show";
import add from "../../app/Ticket/add";
import changeStatus from "../../app/Ticket/changeStatus";


// validation props
const USER_TOKEN = cookie('token').isString()
const USER_CSRF_TOKEN = header('csrf-token').isString()


// validations
const VL = {
    authentication: validator([ USER_TOKEN, USER_CSRF_TOKEN ]),
}


export default Router()
// user details
    .post('/add', VL.authentication, authentication, add)
    .get('/list', VL.authentication, authentication, list)
    .get('/show/:ticketId', VL.authentication, authentication, show)
    .patch('/change-status', VL.authentication, authentication, changeStatus)
    