import {Router} from 'express';
import {body, cookie, header, oneOf} from "express-validator";
import {MIN_PASSWORD_LENGTH, MOBILE_TOKEN_LENGTH} from "../constant";
import authentication from "../../middleware/authentication";
import validator from "../../middleware/validator";
import logout from "../../app/user/logout";
import login from "../../app/user/login";
import register from "../../app/user/register";
import changePassword from "../../app/user/changePassword";
import checkUserExist from "../../app/user/checkUserExist";
import mobileToken_sendSMS from "../../app/user/mobileToken_sendSMS";
import forgetPassword_email from "../../app/user/forgetPassword_email";
import mobileToken_verify from "../../middleware/mobileToken_verify";
import forgetPassword_mobile from "../../app/user/forgetPassword_mobile";
import resetPassword_verify from "../../app/user/resetPassword_verify";
import resetPassword_submit from "../../app/user/resetPassword_submit";
import emailVerify_submit from "../../app/user/emailVerify_submit";
import emailVerify_resend from "../../app/user/emailVerify_resend";
import detail_get from "../../app/user/detail_get";
import detail_update from "../../app/user/detail_update";





// validation props
const MOBILE = body('mobile').isMobilePhone('fa-IR')
const EMAIL = body('email').isEmail()
const PASSWORD = body('password').isString().isLength({min: MIN_PASSWORD_LENGTH})
const NEWPASSWORD = body('newPassword').isString().isLength({min: MIN_PASSWORD_LENGTH})
const MOBILE_TOKEN = body('token').isString().isLength(MOBILE_TOKEN_LENGTH)
const USER_TOKEN = cookie('token').isString()
const USER_CSRF_TOKEN = header('csrf-token').isString()
const VERIFY_HASH = body('verifyHash').isString()
const TEMPLATE = body('template').isString()
const NAME = body('name').isString().optional({nullable: true})
const FAMILY = body('family').isString().optional().optional({nullable: true})
const GENDER = body('gender').custom((value) => value === 'F' || value === 'M').optional({nullable: true})




// validations
const VL = {
    detail_update:         validator([ GENDER, NAME, FAMILY ]),
    authentication:        validator([ USER_TOKEN, USER_CSRF_TOKEN ]),
    checkUserExist:        validator(oneOf([ EMAIL, MOBILE ])),
    mobileToken_sendSMS:   validator([ MOBILE, TEMPLATE ]),
    login:                 validator(oneOf([ [ EMAIL, PASSWORD ], [ MOBILE, PASSWORD ] ])),
    register:              validator([ MOBILE, MOBILE_TOKEN, EMAIL, PASSWORD ]),
    forgetPassword_email:  validator(EMAIL),
    forgetPassword_mobile: validator([ MOBILE, MOBILE_TOKEN ]),
    resetPassword_verify:  validator(oneOf([ [ EMAIL, VERIFY_HASH ], [ MOBILE, VERIFY_HASH ] ])),
    resetPassword_submit:  validator(NEWPASSWORD),
    changePassword:        validator([ PASSWORD, NEWPASSWORD ]),
}





export default Router()
// user details
    .get('/detail/get', VL.authentication, authentication, detail_get)
    .patch('/detail/update', VL.authentication, authentication, VL.detail_update, detail_update)
    
    // update details
    .patch('/change-password', VL.authentication, authentication, VL.changePassword, changePassword)
    
    // user sign
    .post('/login', VL.login, login)
    .post('/register', VL.register, mobileToken_verify, register)
    .post('/logout', VL.authentication, authentication, logout)
    
    // user sign utility
    .post('/check-user-exist', VL.checkUserExist, checkUserExist)
    .post('/mobile-token-send-sms', VL.mobileToken_sendSMS, mobileToken_sendSMS)
    
    // forget password
    .post('/forget-password/email', VL.forgetPassword_email, forgetPassword_email)
    .post('/forget-password/mobile', VL.forgetPassword_mobile, mobileToken_verify, forgetPassword_mobile)
    
    // reset password
    .post('/reset-password/verify', VL.resetPassword_verify, resetPassword_verify)
    .post('/reset-password/submit', VL.resetPassword_verify, VL.resetPassword_submit, resetPassword_submit)
    
    // email link verify
    .post('/email-verify/submit', VL.authentication, authentication, emailVerify_submit)
    .post('/email-verify/resend', VL.authentication, authentication, emailVerify_resend)

