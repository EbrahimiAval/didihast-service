import mongoose from 'mongoose';

export default function dbConnect() {
    mongoose.connect(
        process.env.MONGO_DB_URL,
        {
            useNewUrlParser:    true,
            useUnifiedTopology: true,
            useCreateIndex:     true
        }
    )
    
    const db = mongoose.connection
    db.on('error', console.error.bind(console, 'DB connection error:'))
    db.once('open', async () => {
        console.log('DB succesfully connected.')
        // add and remove a key to schema
        // NOTICE: for and and remove in schem key define must be exsit. in remove after do it can remove schema key from define schema
        // const u = await Session.updateMany({}, {$set: {aaaaaaa: 'dddd'}}); // add
        // const u = await Session.updateMany({}, {$unset: {aaaaaaa: 1 }}); // remove
        // console.log(u)
    })
}