const crypto = require('crypto');

export default function tokenGenerator(bytesNumber) {
    return new Promise((resolve, reject) => {
        crypto.randomBytes(bytesNumber, (err, buf) => {
            if (err) {
                reject(err);
            }
            const token = buf.toString('hex');
            resolve(token);
        })
    })
}