import nodemailer from "nodemailer";


export default async function (to, subject, message, isHtml) {
    const {MAIL_ADDRESS, MAIL_HOST, MAIL_PORT, MAIL_USER, MAIL_TOKEN} = process.env
    const transporter = nodemailer.createTransport({
        ////////// gmail config
        // service: 'gmail',
        // auth:    {
        //     user: GMAIL_ADDRESS,
        //     pass: GMAIL_TOKEN
        // }
        
        ////////// liara config
        host: MAIL_HOST,
        port: MAIL_PORT,
        tls:  true,
        auth: {
            user: MAIL_USER,
            pass: MAIL_TOKEN
        }
    })
    
    
    const mailOptions = {
        from:    {
            name:    'دیدی هست',
            // address: GMAIL_ADDRESS
            address: MAIL_ADDRESS
        },
        to:      to,
        subject: subject
    }
    
    if (!isHtml)
        mailOptions.text = message;
    else
        mailOptions.html = message;
    
    await transporter.sendMail(mailOptions)
}
