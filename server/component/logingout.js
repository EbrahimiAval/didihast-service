import {TOKEN_COOKIE_OPTIONS} from "../setup/constant";

export default async function logingout(req, res) {
    const {session} = req
    await session.expireToken(session.token)
    res.clearCookie('token', {domain: TOKEN_COOKIE_OPTIONS.domain})
}