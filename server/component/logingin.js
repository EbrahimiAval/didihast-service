import {TOKEN_COOKIE_NAME, TOKEN_COOKIE_OPTIONS} from "../setup/constant";
import initSession from "./initSession";

export default async function logingin(res, user) {
    const session = await initSession(user._id);
    
    res.cookie(TOKEN_COOKIE_NAME, session.token, TOKEN_COOKIE_OPTIONS)
       .json({csrfToken: session.csrfToken})
}