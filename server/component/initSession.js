import Session from "../model/Session";



export default async function initSession(userId) {
    const token = await Session.generateToken();
    const csrfToken = await Session.generateToken();
    const session = new Session({token, csrfToken, userId});
    await session.save();
    return session;
};