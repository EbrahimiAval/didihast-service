import {validationResult} from "express-validator";

// handel validation error
export default function validator(validations) {
    
    const inspector = async (req, res, next) => {
        if (!Array.isArray(validations)) {
            if (validations.run)
                await validations.run(req)
            else
                return await validations(req, res, next)
        }
        else {
            await Promise.all(validations.map(validation => validation.run(req)));
        }
        
        return next()
    }
    
    const result = async (req, res, next) => {
        const errors = validationResult(req);
        if (errors.isEmpty())
            return next()
        
        res.status(422).json({errors: errors.array()})
    }
    
    return [ inspector, result ]
}
