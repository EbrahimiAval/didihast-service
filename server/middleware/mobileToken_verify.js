import catcher from "./catcher";
import MobileToken from "../model/MobileToken";
import {MOBILE_TOKEN_EXPIRE} from "../setup/constant";

async function mobileToken_verify(req, res, next) {
    
    const {mobile, token} = req.body;
    const mobileToken = await MobileToken.findOne({token})
    
    if (!mobileToken)
        req.setError(400, 'mobile token is not valid.')()
    
    if ( mobileToken.mobile !== mobile)
        req.setError(401, 'mobile is not valid.')()
    
    const exp = mobileToken.createdAt.getTime() + MOBILE_TOKEN_EXPIRE
    const now = new Date().getTime()
    
    if (exp < now)
        req.setError(402, 'mobile token has expired.')()
    
    next();
}

export default catcher(mobileToken_verify)