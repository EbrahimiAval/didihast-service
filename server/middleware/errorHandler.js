import {IS_DEVELOPMENT, IS_PRODUCTION} from "../setup/constant";

// use req.setError() in controllers to can use this middleware
export default function errorHandler(err, req, res, next) {
    if (!req.customError)
        console.error(err.stack)
    
    const errorMessage =
              IS_DEVELOPMENT || (IS_PRODUCTION && req.status !== undefined) ?
                  err.message || 'Something broke!'
                  :
                  'Something broke!'
    
    res.status(req.status || 500).send(errorMessage)
}
