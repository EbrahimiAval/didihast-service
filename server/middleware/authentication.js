import Session from "../model/Session";
import catcher from "../middleware/catcher";
import logingout from "../component/logingout";
import {TOKEN_COOKIE_OPTIONS, TOKEN_EXPIRE} from "../setup/constant";


async function authentication(req, res, next) {
    const {token} = req.cookies;
    const session = await Session.findOne({token})
    
    // token validation
    if (!session) {
        res.clearCookie('token', {domain: TOKEN_COOKIE_OPTIONS.domain})
        req.setError(401, 'token is not valid.')()
    }
    
   
    req.session = session;  // push session to can access in next middlewares
    const {csrfToken, createdAt} = session
    const exp = createdAt.getTime() + TOKEN_EXPIRE
    const now = new Date().getTime()
    
    // check token expire
    if (exp < now || session.status !== 'valid') {
        await logingout(req, res)
        req.setError(401, 'Your session has expired. You need to log in agin.')()
    }
    
    // csrf validation
    const receivedCsrfToken = req.headers['csrf-token']
    if (!receivedCsrfToken || csrfToken !== receivedCsrfToken)
        req.setError(401, 'Provided CSRF-token is invalid')()
    
    next();
}

export default catcher(authentication)



