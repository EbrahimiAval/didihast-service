export default function setError(req, res, next) {
    
    req.setError = function (status, message, logging = false) {
       
        if (isNaN(Number(status)))
            throw new Error('ERROR (setError middleware) status is not a number type!')
        
        return function (err) {
            if (logging)
                console.error(err.stack)
            
            req.status = status
            req.customError = true
            throw new Error(message || err.message)
        }
    }
    
    next()
}
