import {model, Schema} from "mongoose";
import {MOBILE_TOKEN_LENGTH} from "../setup/constant";
import uniqueValidator from "mongoose-unique-validator";


const schema = new Schema({
    token:     {type: Number, unique: true, required: true},
    createdAt: {type: Date, default: Date.now},
    mobile:    {type: String, required: true}
})

schema.plugin(uniqueValidator);

schema.statics.generateToken = function () {
    let token = "";
    for (let i = 0; i < MOBILE_TOKEN_LENGTH; i++) {
        let num = Math.floor(Math.random() * 10);
        if (i === 0 && num === 0)
            num++; // when convert "01234" string to number get (1234). first zire removed.
        token += num;
    }
    return Number(token);
}

export default model('MobileToken', schema)
