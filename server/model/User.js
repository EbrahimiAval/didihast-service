import {model, Schema} from "mongoose";
import uniqueValidator from "mongoose-unique-validator";
import bcryptjs from "bcryptjs";
import emailVerifyTemplate from "../setup/template/emailVerifyTemplate";
import mailer from "../setup/utility/mailer";
import md5 from "md5";
import forgetPasswordTemplate from "../setup/template/forgetPasswordTemplate";
import {MIN_PASSWORD_LENGTH} from "../setup/constant";



const schema = new Schema({
    name:          {type: String, default: null, trim: true},
    family:        {type: String, default: null, trim: true},
    mobile:        {type: String, required: true, trim: true, unique: true},
    email:         {type: String, required: true, trim: true, unique: true},
    emailVerified: {type: Boolean, default: false},
    gender:        {type: String, default: null},
    password:      {type: String, required: true, minlength: MIN_PASSWORD_LENGTH},
    createdAt:     {type: Date, default: Date.now},
    wallet:        {type: Number, default: 0},
    ban:           {type: Number, default: 0},
    role:          {type: String, default: 'USER'},
})


schema.plugin(uniqueValidator)


// schema.virtual('fullName').get(function() {
//     return this.name + ' ' + this.family;
// })


// hashing user password
schema.pre('save', async function (next) {
    let user = this;
    
    if (!user.isModified('password'))
        return next();
    
    await bcryptjs
        .genSalt(12)
        .then((salt) => {
            return bcryptjs.hash(user.password, salt);
        })
        .then((hash) => {
            user.password = hash;
            next();
        })
})


schema.methods.changePassword = async function (password) {
    this.password = password;
    return await this.save()
}


schema.methods.detailUpdate = async function (params) {
    const canUpdtae = [ 'name', 'family', 'gender' ]
    
    canUpdtae.forEach((item) => {
        if (params[item] === undefined)
            return;
        const IS_EMPTY_STRING = (typeof params[item] === "string" && params[item].trim() === '');
        this[item] = IS_EMPTY_STRING ? null : params[item]
    })
    
    return await this.save()
}



schema.methods.verifyHash = function () {
    const base = this.createdAt + this.password.slice(5, 15)
    const hash = md5(base)
    return hash
}



schema.statics.verifingResetPasswordToken = async function (req) {
    const {email, mobile, verifyHash} = req.body;
    const username = email ? {email} : {mobile}
    const user = await this.findOne(username)
    const sendBadReq = req.setError(403, 'verification faild.')
    if (!user)
        sendBadReq()// To prevent the disclosure of information
    
    const validHash = user.verifyHash()
    if (verifyHash !== validHash)
        sendBadReq()
    
    return user;
}



schema.methods.setEmailVerified = function () {
    return this.updateOne({$set: {emailVerified: true}})
}



schema.methods.sendEmailVerifyLink = async function (req) {
    const verifyHash = await this.verifyHash()
    const link = req.get('origin') + '/login-register/email-verify/' + verifyHash
    const template = emailVerifyTemplate(link);
    const subject = 'تایید ایمیل حساب کاربری';
    return await mailer(this.email, subject, template, true)
}



schema.methods.sendForgetPasswordLink = async function (req) {
    const verifyHash = await this.verifyHash()
    const link = req.get('origin') + '/login-register/reset-password/' + this.email + '/' + verifyHash
    const template = forgetPasswordTemplate(link);
    const subject = 'بازیابی رمز عبور';
    return await mailer(this.email, subject, template, true)
}


export default model('User', schema);
