import {model, Schema} from "mongoose";
import tokenGenerator from "../setup/utility/tokenGenerator";
import uniqueValidator from "mongoose-unique-validator"


const schema = new Schema({
    token:     {type: String, unique: true, required: true},
    csrfToken: {type: String, unique: true, required: true},
    createdAt: {type: Date, default: Date.now},
    userId:    {type: Schema.Types.ObjectId, required: true, ref: 'User'},
    status:    {type: String, enum: [ 'valid', 'expired' ], default: 'valid'}
})


schema.plugin(uniqueValidator);


schema.statics.generateToken = function () {
    return tokenGenerator(16)
}


schema.statics.expireAllTokensForUser = function (userId) {
    return this.updateMany({userId}, {$set: {status: 'expired'}})
}



schema.methods.expireToken = function () {
    return this.updateOne({$set: {status: 'expired'}})
}


export default model('Session', schema)
