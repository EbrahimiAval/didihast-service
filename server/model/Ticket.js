import {model, Schema} from "mongoose";
import uniqueValidator from "mongoose-unique-validator";


const commentsSchema = new Schema({
    description: {type: String, required: true, trim: true},
    isUser:      {type: Boolean, default: true},
    createdAt:   {type: Date, default: Date.now}
}, {_id: false})


const schema = new Schema({
    shortId:   {type: Number},
    userId:    {type: Schema.Types.ObjectId, ref: 'User', required: true},
    subject:   {type: String, default: null, trim: true},
    status:    {type: String, default: 'OPEN'},
    createdAt: {type: Date, default: Date.now},
    comments:  [ commentsSchema ]
})

schema.plugin(uniqueValidator)


schema.pre('save', function (next) {
    // NOTICE: don't ever remove documents. shortId is id of ticket for improve UX.
    if (this.isNew)
        Ticket.countDocuments().then(res => {
            this.shortId = res // Increment count
            next()
        })
    else
        next()
})

const Ticket = model('Ticket', schema)

export default Ticket;
